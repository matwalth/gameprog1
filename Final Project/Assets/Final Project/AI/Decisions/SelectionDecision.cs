using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/SelectionDecision", fileName = "Selection Decision")]
public class SelectionDecision : Decision
{
    [SerializeField] private List<Decision> decisions;

    public override bool Decide(StateController controller)
    {
        //bool targetVisible = Look(controller);
        for(int i = 0; i<decisions.Count; i++)
        {
            if (decisions[i].Decide(controller))
            {
                return true;
            }
        }
        return false;
    }
}
