using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Arrived", fileName = "Arrived Decision")]
public class ArrivedDecision : Decision
{

    public override bool Decide(StateController controller)
    {
        return Arrived(controller);
    }
    private bool Arrived(StateController controller)
    {
        if (controller.navMesAgent.isStopped)
        {
            controller.enemyAnimator.SetFloat("Move", 0);
        }
        else
        {
            controller.enemyAnimator.SetFloat("Move", controller.navMesAgent.velocity.magnitude);
        }
        
        //Debug.Log(controller.navMesAgent.remainingDistance);
        if (controller.navMesAgent.remainingDistance < 0.0025f && controller.navMesAgent.remainingDistance != 0)
        //if (Vector3.Distance(controller.transform.position, controller.patrolPoints[controller.patrolIndex].position) < 0.0025f)
        {
            //Debug.Log("Arrived");
            //Debug.Log(controller.navMesAgent.destination);
            //Debug.Log(controller.navMesAgent.remainingDistance);

            controller.patrolIndex = (controller.patrolIndex + 1) % controller.patrolPoints.Count;
            return true;
        }
        return false;
    }
}
