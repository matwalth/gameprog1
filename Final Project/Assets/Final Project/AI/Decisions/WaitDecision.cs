using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Wait", fileName = "Wait Decision")]
public class WaitDecision : Decision
{
    [SerializeField] private float timeToWait = 5;

    public override bool Decide(StateController controller)
    {
        return Wait(controller);
    }
    private bool Wait(StateController controller)
    {
        if(controller.waitTime >= timeToWait)
        {
            //Debug.Log("Done Waiting");
            //controller.waitTime = 0;
            return true;
        }
        

        return false;
    }
}
