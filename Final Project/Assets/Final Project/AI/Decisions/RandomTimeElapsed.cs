using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/RandomTimeElapsed", fileName = "RandomTimeElapsed Decision")]
public class RandomTimeElapsed : Decision
{
    


    public override bool Decide(StateController controller)
    {
        bool targetVisible = TimeElapsed(controller);
        return targetVisible;
    }
    private bool TimeElapsed(StateController controller)
    {
        

        if(controller.time >= controller.randTime)
        {
            return true;
        }
        return false;
    }
}
