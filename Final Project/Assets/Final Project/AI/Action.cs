using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Action : ScriptableObject
{
    //Function called to perform this action
    public abstract void Act(StateController controller);
}
