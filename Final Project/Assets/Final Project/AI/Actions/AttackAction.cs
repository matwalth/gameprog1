using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Attack", fileName = "Attack Action")]
public class AttackAction : Action
{
    public override void Act(StateController controller)
    {
        //Call our interal go to patrol point function
        Attack(controller);
    }
    //Does nothing just plays idle animation
    private void Attack(StateController controller)
    {
        controller.navMesAgent.isStopped = true;
        //animator.SetTrigger("Attack");
    }

}
