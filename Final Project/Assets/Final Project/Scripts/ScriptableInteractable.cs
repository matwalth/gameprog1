using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableInteractable : MonoBehaviour, IInteractable
{
    [Tooltip("Interact Behavior that defines the behavior to execute when interacted with")]
    [SerializeField] private InteractBehavior interactBehavior;




    public void Interact(PlayerInteractManager pim, PlayerController pc)
    {
        //Tell interactBehavior ScriptObject to do its sInteract functionality
        interactBehavior.Interact();
    }
}
