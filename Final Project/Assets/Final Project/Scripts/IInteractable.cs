using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
    
    void Interact(PlayerInteractManager pim, PlayerController pc);
}
