using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightPulser : MonoBehaviour
{
    [Tooltip("The material we are accessing (Drag it here)")]
    [SerializeField] private Material material;

    private int emissionID;

    private void Awake()
    {
        emissionID = Shader.PropertyToID("_Emission");

    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //material.SetVector(emissionID, new Vector3(Mathf.Sin(Time.time % 1), 0, 0));
        material.SetColor(emissionID, Color.yellow * Mathf.Sin(Time.time % 1) * 10f);
        //Debug.Log(Mathf.Sin(Time.time % 1));
    }
}
