using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorPlatform : MonoBehaviour, IInteractable
{
    [SerializeField] Animator animator;
    private bool interactState = false;
    public void Interact(PlayerInteractManager pim, PlayerController pc)
    {
        animator.SetTrigger("Interacted");
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
