using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class LouderSFX : MonoBehaviour
{
    [SerializeField] private GameObject SFX;
    [SerializeField] private AudioClip diedSFX;
    [SerializeField] private AudioClip hurtSFX;
/*    [SerializeField] private AudioClip runSFX;
    [SerializeField] private AudioClip jumpSFX;*/
    [SerializeField] private AudioClip landsSFX;
    [SerializeField] private AudioSource audioSource;


    private void Update()
    {


    }
    private void PlaySFX(AudioClip clipToPlay)
    {
        //If clipToPlay is already playing running while currently running then do nothing
        //if (clipToPlay == audioSource.clip) return;
        //Stop currently playing sound if any
        //audioSource.Stop();
        //Set what clip to play
        audioSource.clip = clipToPlay;

        //How do I make the audio clip play lounder - Cant hear landing SFX
        //audioSource.outputAudioMixerGroup.audioMixer.
        //Play the sound
        audioSource.Play();
        //Debug.Log("Playing SFX");
    }

    public void Death()
    {
        PlaySFX(diedSFX);
    }
    public void Hurt()
    {
        //Debug.Log("Hurt SFX");
        PlaySFX(hurtSFX);
    }
    public void Land()
    {
        Debug.Log("Playing Landing SFX");
        PlaySFX(landsSFX);
        audioSource.loop = false;
        SFX.GetComponent<SFXChanger>().Land();
    }
}
