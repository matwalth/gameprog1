using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
public class PlayerController : MonoBehaviour
{
    [Header("Player Input")]
    [Tooltip("Movement input from our player")]
    [SerializeField] private Vector2 movementInput;

    [Tooltip("Movement input that's aligned with the camera direction")]
    [SerializeField] private Vector3 cameraAdjustedInputDirection;

    //Referebce to out input actions object
    private PlayerInputActions playerInputActions;

    [Header("Camera")]
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private Transform cameraOrientation;

    [Header("Component/Object Reference")]
    [Tooltip("Reference to our movement component (Drag is here)")]
    [SerializeField] private BaseMovement characterMovement;

    [Tooltip("The interaction manager (Drag it here)")]
    [SerializeField] private PlayerInteractManager interactManager;

    [Tooltip("UI used for Interaction")]
    [SerializeField] private UIDocument UIDoc;
    private bool pauseState = false;

    [Header("UI GameObjects")]
    [SerializeField] private GameObject pauseUI;
    [SerializeField] private GameObject hud;
    [SerializeField] private GameObject startMenu;
    [SerializeField] private GameObject wonMenu;
    [SerializeField] private GameObject lostMenu;
    [SerializeField] private AudioSource SFX;
    [SerializeField] private AudioSource BGM;
    [SerializeField] private AudioSource loudSFX;
    private void RotateCameraAndCharacter()
    {
        //Rotate orientation to match the camera's orientation
        Vector3 basicViewDir = transform.position - new Vector3(cameraTransform.position.x, transform.position.y, cameraTransform.position.z);

        //Set this object's forward direction
        cameraOrientation.forward = basicViewDir.normalized;

        //Now rotate the character
        characterMovement.RotateCharacter();
    }

    private void CalculateCameraRelativeInput()
    {
        cameraAdjustedInputDirection = cameraOrientation.forward * movementInput.y + cameraOrientation.right * movementInput.x;

        //Possibly normalize if our vector is too big
        if(cameraAdjustedInputDirection.sqrMagnitude > 1)
        {
            cameraAdjustedInputDirection = cameraAdjustedInputDirection.normalized;
        }
    }
    private void InteractionPerformed(InputAction.CallbackContext context)
    {
        //Call interact with the manager
        interactManager.Interact();
    }
    private void MoveActionPerformed(InputAction.CallbackContext context)
    {
        //Get the user control input as a Vector
        movementInput = context.ReadValue<Vector2>();
        
        //Get the relative inout direction
        CalculateCameraRelativeInput();

        //Now actually move the charater using the movement component
        characterMovement.Move(cameraAdjustedInputDirection);
    }
    private void JumpActionPerformed(InputAction.CallbackContext context)
    {
        characterMovement.Jump();
    }
    private void JumpActionCanceled(InputAction.CallbackContext context)
    {
        characterMovement.JumpCanceled();
    }
    private void ShowUI()
    {
        UIDoc.rootVisualElement.style.visibility = Visibility.Visible;
    }
    private void HideUI()
    {
        UIDoc.rootVisualElement.style.visibility = Visibility.Hidden;
    }
    private void SubscribeInputActions()
    {
        playerInputActions.Player.Move.started += MoveActionPerformed;
        //Subscribe to the rest of the actions that you need to
        playerInputActions.Player.Move.canceled += MoveActionPerformed;
        playerInputActions.Player.Move.performed += MoveActionPerformed;

        playerInputActions.Player.Jump.canceled += JumpActionCanceled;
        playerInputActions.Player.Jump.performed += JumpActionPerformed;

        playerInputActions.Player.Pause.performed += OnPlayerPause;
        playerInputActions.UI.Escape.performed += UIEscapePause;
        playerInputActions.Player.Interact.performed += InteractionPerformed;

        interactManager.OnInteractablesExist.AddListener(ShowUI);
        interactManager.OnInteractablesDoNotExist.AddListener(HideUI);
    }
    private void UnSubscribeInutAction()
    {
        playerInputActions.Player.Move.started -= MoveActionPerformed;
        playerInputActions.Player.Move.canceled -= MoveActionPerformed;
        playerInputActions.Player.Move.performed -= MoveActionPerformed;

        playerInputActions.Player.Jump.canceled -= JumpActionCanceled;
        playerInputActions.Player.Jump.performed -= JumpActionPerformed;

        playerInputActions.Player.Interact.performed -= InteractionPerformed;

        playerInputActions.Player.Pause.performed -= OnPlayerPause;
        playerInputActions.UI.Escape.performed -= UIEscapePause;
    }
    public void StartScreen()
    {
        SwitchActionMap("UI");
    }
    public void GameStart()
    {
        SwitchActionMap("Player");
    }
    /*private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Hitting something");
        if (other.CompareTag("Finish"))
        {
            Won();
        }
    }*/
    public void Won()
    {
        //Play win music

        GameManager._instance.GameWon();
        SFX.GetComponent<SFXChanger>().StopSFX();
        SwitchActionMap("UI");
        Debug.Log("You have Won!");
        BGM.GetComponent<BGMChanger>().Victory();
    }
    public void Kill()
    {


        if (hud.GetComponent<HUDController>().GetLives() == 0)
        {

            Death();
        }
        else
        {
            loudSFX.GetComponent<LouderSFX>().Hurt();
            //hud.GetComponent<HUDController>().Hurt();
            GameManager._instance.Hurt();
            Debug.Log("You were Hurt!");
            if (hud.GetComponent<HUDController>().GetLives() == 0)
            {

                Death();
            }
        }
    }
    public void Death()
    {
        loudSFX.GetComponent<LouderSFX>().Death();
        GameManager._instance.GameLost();
        SFX.GetComponent<SFXChanger>().StopSFX();
        SwitchActionMap("UI");
        Debug.Log("You have died!");
        BGM.GetComponent<BGMChanger>().Defeat();
    }
    public void OnPlayerPause (InputAction.CallbackContext context)
    {
        //if (context.performed)
        //{
        if (!pauseState)
        {
            SwitchActionMap("UI");

            pauseState = true;

            GameManager._instance.PauseGame();
        }

    }
    public void EscapePause()
    {
        if (pauseState)
        {
            SwitchActionMap("Player");
            pauseState = false;
            GameManager._instance.ResumeGame();
        }
    }
    public void UIEscapePause(InputAction.CallbackContext context)
    {
        if (pauseState)
        {
            SwitchActionMap("Player");
            pauseState = false;
            GameManager._instance.ResumeGame();
        }
    }
    private void SwitchActionMap(string mapName)
    {
        switch (mapName)
        {

            default:
                playerInputActions.UI.Disable();
                playerInputActions.Player.Enable();
                break;
            case "Player":
                playerInputActions.UI.Disable();
                playerInputActions.Player.Enable();
                break;
            case "UI":
                playerInputActions.UI.Enable();
                playerInputActions.Player.Disable();
                break;
        }
    }
    private void Awake()
    {
        playerInputActions = new PlayerInputActions();

        SubscribeInputActions();

        SwitchActionMap("Player");
    }
    private void OnDestroy()
    {
        UnSubscribeInutAction();
    }
    // Start is called before the first frame update
    void Start()
    {
        UIDoc.rootVisualElement.style.visibility = Visibility.Hidden;
        GameManager._instance.OnGameBegin.AddListener(GameStart);
        GameManager._instance.OnAttack.AddListener(Kill);
    }

    // Update is called once per frame
    void Update()
    {
        //Call our rotation
        RotateCameraAndCharacter();

        //Calculate the new relative input
        CalculateCameraRelativeInput();
    }

    private void FixedUpdate()
    {
        characterMovement.Move(cameraAdjustedInputDirection);
    }
}
