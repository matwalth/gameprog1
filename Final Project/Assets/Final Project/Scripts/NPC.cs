using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{


    



    [Header("NPC Animation")]
    //[SerializeField] private Rigidbody2D npcRigidbody;
    [SerializeField] private CapsuleCollider capsuleCollider;
    [SerializeField] private Animator animator;


    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            animator.SetBool("Player",true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            animator.SetBool("Player", false);
        }
    }
}
