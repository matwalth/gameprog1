using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
public class PauseMenuController : MonoBehaviour
{

    [SerializeField] private UIDocument UIDoc;
    private VisualElement menu;
    private Button quit;
    private Button exit;
    private bool pauseStatus = false;
    [SerializeField] GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        GameManager._instance.OnGamePaused.AddListener(OnPlayerPause);
        GameManager._instance.OnGameResumed.AddListener(EscapePause);
        GameManager._instance.OnGameResumed.AddListener(ButtonEscapePause);
        UIDoc.rootVisualElement.style.visibility = Visibility.Hidden;
        VisualElement root = UIDoc.rootVisualElement;
        menu = root.Q<VisualElement>("pause");
        quit = root.Q<Button>("quit-button");
        exit = root.Q<Button>("resume-button");

        exit.clicked += ButtonEscapePause;
        quit.clicked += QuitButton;
    }
    private void OnDestroy()
    {
        quit.clicked -= QuitButton;
        exit.clicked -= ButtonEscapePause;
    }
    private void QuitButton()
    {
        Debug.Log("Quit Button was Pressed");
        //ButtonEscapePause();
        //player.GetComponent<Lara_Controls>().QuitGame();
        GameManager._instance.QuitGame();
        EscapePause();
        
    }

    public void OnPlayerPause()
    {
       // Debug.Log("Paused Screen");
        if (!pauseStatus)
        {
            Time.timeScale = 0;
            Debug.Log("Showing Pause Screen");
            //Show the pause menu
            UIDoc.rootVisualElement.style.visibility = Visibility.Visible;
            pauseStatus = true;
            
        }
        /*else
        {
            Debug.Log("Hiding Pause Screen");
            //pauseStatus = false;
            //You can hide it using Visibility.hidden
            //UIDoc.rootVisualElement.style.visibility = Visibility.Hidden;
        }*/
    }
    public void ButtonEscapePause()
    {
        //Debug.Log("Paused Screen");
        if (pauseStatus)
        {
            Time.timeScale = 1;
            player.GetComponent<PlayerController>().EscapePause();
            Debug.Log("Hiding Pause Screen from Button");
            //Show the pause menu
            UIDoc.rootVisualElement.style.visibility = Visibility.Hidden;
            pauseStatus = false;
            
        }
    }
    public void EscapePause()
    {
        //Debug.Log("Paused Screen");
        if (pauseStatus)
        {
            Time.timeScale = 1;
            
            Debug.Log("Hiding Pause Screen");
            //Show the pause menu
            UIDoc.rootVisualElement.style.visibility = Visibility.Hidden;
            pauseStatus = false;
        }
    }
}
