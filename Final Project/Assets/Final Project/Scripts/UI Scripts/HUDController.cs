using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.Events;
public class HUDController : MonoBehaviour
{
    [Tooltip("Reference to the UIDocument object")]
    [SerializeField] private UIDocument UIDoc;
    //private float levelTime;
    [SerializeField] private Sprite heartImage;

    //Reference to teh hearts container to add heart to
    private VisualElement heartsContainer;
    //Reference to the label for time
    //private Label timeLabel;
    //private float timerPaused;
    private int lives;

    // Start is called before the first frame update
    void Start()
    {
        GameManager._instance.OnGamePaused.AddListener(OnPlayerPause);
        GameManager._instance.OnGameResumed.AddListener(EscapePause);
        GameManager._instance.OnHurt.AddListener(Hurt);
        GameManager._instance.OnGameStart.AddListener(Restart);
        GameManager._instance.OnHealth.AddListener(GainLife);
        //levelTime = GetComponent<Timer>().TimeLeft();
        //Grab the root of the UI Document
        VisualElement root = UIDoc.rootVisualElement;
        //Name of the actual element for time
        //timeLabel = root.Q<Label>("time-left-value");

        //Name of the container for hearts
        heartsContainer = root.Q<VisualElement>("lives");


        //timeLabel.text = "" + levelTime;

        //StartCoroutine(TimerCountdown());
        //Show three lives
        //GameManager._instance.lives = 3;
        lives = GameManager._instance.lives;
        for (int i = 0; i < lives; i++)
        {
            AddLife(heartsContainer);
        }

        //UnityEngine.Cursor.visible = true;
        //UnityEngine.Cursor.lockState = CursorLockMode.Confined;

    }
    public void Restart()
    {
        //levelTime = GetComponent<Timer>().TimeLeft();
        //Grab the root of the UI Document
        VisualElement root = UIDoc.rootVisualElement;
        //Name of the actual element for time
        //timeLabel = root.Q<Label>("time-left-value");

        //Name of the container for hearts
        heartsContainer = root.Q<VisualElement>("lives");


        //timeLabel.text = "" + levelTime;

        //StartCoroutine(TimerCountdown());
        int l = GameManager._instance.lives;
        for(int i = 0; i<l; i++)
        {
            RemoveLife(heartsContainer);
        }
        //Show three lives
        GameManager._instance.lives = 3;
        lives = GameManager._instance.lives;
        for (int i = 0; i < lives; i++)
        {
            AddLife(heartsContainer);
        }
        
    }
    //private IEnumerator TimerCountdown(){}

    // Update is called once per frame
    void Update()
    {
        //levelTime = GetComponent<Timer>().TimeLeft();
        //timeLabel.text = "" + levelTime;
    }
    public void GainLife()
    {
        AddLife(heartsContainer);
    }
    public void AddLife(VisualElement container)
    {
        //Create a new Image
        Image heart = new Image();
        //Settung the sprite for the image
        heart.sprite = heartImage;

        //Set up the default style information
        heart.style.paddingTop = 5;
        heart.style.paddingLeft = 0;
        heart.style.paddingRight = 0;
        heart.style.width = 32;
        heart.style.height = 32;
        heart.style.flexGrow = 0;
        heart.style.flexShrink = 0;
        
        container.Add(heart);
    }
    public int GetLives()
    {
        return lives;
    }
    public void Hurt()
    {
        RemoveLife(heartsContainer);
    }
    public void RemoveLife(VisualElement container)
    {
        //Check to see if there are lives to be removed
        //if(container.)
            container.RemoveAt(0);
            lives--;
        //When on last life trigger Player Death()
    }

    public void OnPlayerPause()
    {
        //GetComponent<Timer>().PauseTimer(levelTime);
        //Pause timer
        //timerPaused = levelTime;
        //Debug.Log("Timer paused @: "+timerPaused);
        UIDoc.rootVisualElement.style.visibility = Visibility.Hidden;
    }
    public void EscapePause()
    {
        //GetComponent<Timer>().StartTimer();
        //Resume timer
        //levelTime = timerPaused;
        //Debug.Log("Resuming timer @: " + levelTime);
        //timeLabel.text = "" + levelTime;
        UIDoc.rootVisualElement.style.visibility = Visibility.Visible;
     
    }
}
