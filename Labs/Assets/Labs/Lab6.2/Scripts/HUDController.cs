using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.Events;
public class HUDController : MonoBehaviour
{
    [Tooltip("Reference to the UIDocument object")]
    [SerializeField] private UIDocument UIDoc;
    [SerializeField] private Timer time;
    private float levelTime;
    [SerializeField] private Sprite heartImage;
    private Button addHeart;
    private Button removeHeart;
    //Reference to teh hearts container to add heart to
    private VisualElement heartsContainer;
    //Reference to the label for time
    private Label timeLabel;
    private float timerPaused;

    // Start is called before the first frame update
    void Start()
    {
        levelTime = GetComponent<Timer>().TimeLeft();
        //Grab the root of the UI Document
        VisualElement root = UIDoc.rootVisualElement;
        //Name of the actual element for time
        timeLabel = root.Q<Label>("time-left-value");

        //Name of the container for hearts
        heartsContainer = root.Q<VisualElement>("lives");

        addHeart = root.Q<Button>("add-life");
        removeHeart = root.Q<Button>("remove-life");
        timeLabel.text = "" + levelTime;

        //StartCoroutine(TimerCountdown());
        //Show three lives
        AddLife(heartsContainer);
        AddLife(heartsContainer);
        AddLife(heartsContainer);
     /*   UnityEngine.Cursor.visible = true;
        UnityEngine.Cursor.lockState = CursorLockMode.Confined;*/
        RemoveLife(heartsContainer);
        addHeart.clicked += ButtonFunction;
        removeHeart.clicked += ButtonFunction2;
    }
    //private IEnumerator TimerCountdown(){}
    private void OnDestroy()
    {
        //demoButton.clicked -= ButtonFunction;
        addHeart.clicked -= ButtonFunction;
        removeHeart.clicked -= ButtonFunction2;
    }
    private void ButtonFunction()
    {
        AddLife(heartsContainer);
    }
    private void ButtonFunction2()
    {
        RemoveLife(heartsContainer);
    }
    // Update is called once per frame
    void Update()
    {
        levelTime = GetComponent<Timer>().TimeLeft();
        timeLabel.text = "" + levelTime;
    }

    public void AddLife(VisualElement container)
    {
        //Create a new Image
        Image heart = new Image();
        //Settung the sprite for the image
        heart.sprite = heartImage;

        //Set up the default style information
        heart.style.paddingTop = 5;
        heart.style.paddingLeft = 0;
        heart.style.paddingRight = 0;
        heart.style.width = 32;
        heart.style.height = 32;
        heart.style.flexGrow = 0;
        heart.style.flexShrink = 0;

        container.Add(heart);
    }
    public void RemoveLife(VisualElement container)
    {
        container.RemoveAt(0);
    }

    public void OnPlayerPause()
    {
        GetComponent<Timer>().PauseTimer(levelTime);
        //Pause timer
        timerPaused = levelTime;
        Debug.Log("Timer paused @: "+timerPaused);
        UIDoc.rootVisualElement.style.visibility = Visibility.Hidden;
    }
    public void EscapePause()
    {
        GetComponent<Timer>().StartTimer();
        //Resume timer
        levelTime = timerPaused;
        Debug.Log("Resuming timer @: " + levelTime);
        timeLabel.text = "" + levelTime;
        UIDoc.rootVisualElement.style.visibility = Visibility.Visible;
     
    }
}
