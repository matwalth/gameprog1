using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelTransition : MonoBehaviour
{
    [SerializeField] private string levelToTransitionTo;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if the object that entered this trigger is the player
        if (collision.gameObject.CompareTag("Player"))
        {
            SceneManager.LoadScene(levelToTransitionTo);
        }
    }
}
