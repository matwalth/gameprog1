using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class SFXChanger : MonoBehaviour
{
    [SerializeField] private AudioClip victorySFX;
    [SerializeField] private AudioClip defeatSFX;

    [SerializeField] private AudioSource audioSource;


    private void Update()
    {
        //Play audio associated with key press
        Keyboard kb = Keyboard.current;
        if (kb.digit1Key.wasPressedThisFrame)
        {
            PlaySFX(victorySFX);
        }
        else if (kb.digit2Key.wasPressedThisFrame)
        {
            PlaySFX(defeatSFX);
        }
    }
    private void PlaySFX(AudioClip clipToPlay)
    {
        //If clipToPlay is already playing then do nothing
        if (clipToPlay == audioSource.clip) return;
        //Stop currently playing sound if any
        audioSource.Stop();
        //Set what clip to play
        audioSource.clip = clipToPlay;
        //Play the sound
        audioSource.Play();
    }

}
