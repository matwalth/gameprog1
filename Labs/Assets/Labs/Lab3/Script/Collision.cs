using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private Rigidbody2D body;
    [SerializeField]
    private BoxCollider2D collide;

    private GameObject floor;
    // Start is called before the first frame update
    void Start()
    {
        if (animator == null || body == null || collide == null)
        {
            Debug.LogError("You forgot to set a Component");
        }
        floor = GameObject.Find("Floor");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //Just hit another collider 2D
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Set collision param to enter push animation
        if(collision.gameObject != floor)
            animator.SetBool("Collision",true);
    }
    //Hitting a collider 2D
    private void OnCollisionStay2D(Collision2D collision)
    {
        //Do something
    }

    //Just stop hitting a collider 2D
    private void OnCollisionExit2D(Collision2D collision)
    {
        //Change collision param to exit push animation
        if (collision.gameObject != floor)
            animator.SetBool("Collision", false);
    }
}
