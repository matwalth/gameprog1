using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerLogger : MonoBehaviour
{
    private void Start()
    {
        //Only using GetComponent for demo purposes
        Timer timer = GetComponent<Timer>();
        timer.StartTimer();
    }
    public void TimerStarted(Timer t)
    {
        Debug.Log("Timer started with " + t.Duration + " seconds");
    }
    public void TimerExpired(Timer t)
    {
        Debug.Log("Timer expired");
    }
    public void TimerTick(Timer t)
    {
        Debug.Log("Timer ticked with " + t.TimeLeft() + " seconds left");
    }
}
