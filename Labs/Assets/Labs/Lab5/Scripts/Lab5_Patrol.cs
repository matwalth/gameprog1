using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lab5_Patrol : MonoBehaviour
{

    [Header("Enemy Movement")]
    [SerializeField] private float moveSpeed = 0.8f;
    //[SerializeField] private float patrolTime = 2;
    [SerializeField] private List<Vector2> points = new List<Vector2>() { new Vector2(1, 0), new Vector2(3, 0) };
    private int index = 0;

    private enum Direction
    {
        Right, Left
    }
    private Direction moveDirection;
    [SerializeField] private bool enemyPatrolling = true;

    [Header("Enemy Animation")]
    [SerializeField] private Rigidbody2D enemyRigidbody;
    [SerializeField] private BoxCollider2D boxCollider2D;
    [SerializeField] private SpriteRenderer spriteRenderer;


    private void Move()
    {
        Vector2 targetPosition = transform.position;

        switch (moveDirection)
        {
            default:
                //targetPosition += (points[index] - targetPosition) * moveSpeed * Time.fixedDeltaTime;
                //break;
            case Direction.Right:
                targetPosition += Vector2.right * moveSpeed * Time.fixedDeltaTime;
                break;
            case Direction.Left:
                targetPosition -= Vector2.right * moveSpeed * Time.fixedDeltaTime;
                break;
        }

        enemyRigidbody.MovePosition(targetPosition);
    }
    private void FlipMoveDirection()
    {
        if (moveDirection == Direction.Right)
        {
            moveDirection = Direction.Left;
            spriteRenderer.flipX = true;
        }
        else
        {
            moveDirection = Direction.Right;
            spriteRenderer.flipX = false;
        }
    }

    private IEnumerator EnemyPatrol()
    {
        //while (EnemyPatrolling)
        //{
        //    float counter = 0f;
        //    while (counter < patrolTime)
        //    {
        //        counter += Time.fixedDeltaTime;
        //        Move();
        //        yield return new WaitForFixedUpdate();
        //    }
        //    yield return new WaitForSeconds(5);
        //    FlipMoveDirection();
        //}
        while (enemyPatrolling)
        {
            int length = points.Count;
            for (int i = 0; i < length; i++)
            {
                index = i;
                while (Vector2.Distance(transform.position, points[i]) > 0.1f)
                {
                    //Debug.Log("Almost at point "+i);
                    // Move the enemy
                    Move();

                    yield return new WaitForFixedUpdate();
                }
                //Debug.Log("Arrived at point " + i);
                yield return new WaitForSeconds(5);

                // Flip after patrol time has expired
                FlipMoveDirection();
            }
            
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // Start the Coroutine for the enemy 
        StartCoroutine(EnemyPatrol());
    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            // other.get
        }
    }
}
