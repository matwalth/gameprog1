using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events; // needed for events
public class Timer : MonoBehaviour
{
    [Header("Timer Configuration")]
    [SerializeField] float timerDuration;

    private bool timerStarted;
    private float endTime;

    [Header("Timer Events")]
    [SerializeField] private UnityEvent<Timer> OnTimerStarted;
    [SerializeField] private UnityEvent<Timer> OnTimerExpired;
    [SerializeField] private UnityEvent<Timer> OnTimerTick;

    // Property to see if the Timer has started
    public bool TimerStarted { get { return timerStarted; } }
    public float Duration { get { return timerDuration; } set { timerDuration = value; } }

    //Return how much time is left
    public float TimeLeft()
    {
        float left = endTime - Time.time;
        if(left <= 0)
        {
            return 0;
        }
        else
        {
            return left;
        }
    }
    
    public void StartTimer()
    {
        //Debug.Log("Timer StartTimer()");
        if (!TimerStarted)
        {
            timerStarted = true;
            StartCoroutine(StartCountdown());
        }
    }
    
    public void PauseTimer(float t)
    {
        //Debug.Log("Timer PauseTimer()");
        timerDuration = t;
        timerStarted = false;
        StopCoroutine(StartCountdown());
    }
    protected virtual IEnumerator StartCountdown()
    {
        // Calculate the end time
        endTime = Time.time + Duration;
        OnTimerStarted.Invoke(this);

        while(Time.time < endTime)
        {
            OnTimerTick.Invoke(this);
            //At this point your computation is suspended
            yield return null;
            //Execution will later return here
        }
        OnTimerExpired.Invoke(this);
        //End of the Coroutine
        yield return null;
    }
}
