using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SimplePlayerControl2 : MonoBehaviour
{

    private Keyboard key;
    [SerializeField]
    private float forceMultiplier = 10.0f;

    [SerializeField]
    private Vector2 direction;

    private int side = 1;
    // Keeps a reference to the RigidBody
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    // Awake is called when the script is first initialized
    private void Awake()
    {
        // Store the current Rigidbody
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        key = Keyboard.current;
    }

    // Update is called once per frame
    void Update()
    {
        bool somethingPressed = false;
        // Check to see if key was pressed
        if (key.aKey.isPressed)
        {
            // Debug.Log("Any key was pressed");
            direction = new Vector2(-1.0f, 0.0f);
            somethingPressed = true;
            if(side == 0)
                sr.flipX = true;
            side = 1;
        }
        if(key.dKey.isPressed)
        {
            direction = new Vector2(1.0f, 0.0f);
            somethingPressed = true;
            
            if(side == 1)
                sr.flipX = false;
            side = 0;
        }
        if (key.spaceKey.isPressed)
        {
            Debug.Log("Space");
            direction = new Vector2(0.0f, 2.5f);
            somethingPressed = true;
        }
        if (!somethingPressed)
            direction = Vector2.zero;

        
    }

    private void FixedUpdate()
    {
        rb.AddForce(direction * forceMultiplier);
        
    }
}
