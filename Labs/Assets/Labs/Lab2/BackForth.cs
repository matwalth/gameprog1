using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackForth : MonoBehaviour
{
    [SerializeField]
    private float speed = 1.0f;
    [SerializeField]
    private List<Vector3> points = new List<Vector3>() {new Vector3(1,0,0), new Vector3(3,0,0)};

    private Vector2 velocity;
    private Rigidbody2D rb2D;
    private int dir = 1;
    // Start is called before the first frame update
    void Awake()
    {
        rb2D = gameObject.GetComponent<Rigidbody2D>();
        velocity = new Vector2(speed,0.0f);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void FixedUpdate()
    {
        
        rb2D.MovePosition(rb2D.position + velocity * Time.fixedDeltaTime);
        if ((Vector3.Distance(rb2D.position, points[1]) < 0.001f && dir > 0) || (Vector3.Distance(rb2D.position, points[0]) < 0.001f && dir < 0))
        {
            // Swap the direction.
            velocity *= -1;
            dir *= -1;
            // print("Change: "+dir);
        }
    }
}
