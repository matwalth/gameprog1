using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrolling : MonoBehaviour
{
    [SerializeField]
    private float speed = 1.0f;
    [SerializeField]
    private List<Vector3> points = new List<Vector3>() {new Vector3(1,0,0), new Vector3(3,0,0)};
    private int dir = 1;
    // Start is called before the first frame update
    void Start()
    {
        // points.Add(new Vector3(1,0,0));
        // points.Add(new Vector3(3,0,0));
    }

    // Update is called once per frame
    void Update()
    {
        var step = speed * Time.deltaTime;
        
        //gameObject.transform.Translate(-speed * Time.deltaTime,0,0);

        //print("dir: " + dir);

        if(dir > 0)
        {
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, points[1], step);
        }
        else
        {

            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, points[0], step);
        }
        if (Vector3.Distance(gameObject.transform.position, points[1]) < 0.001f || Vector3.Distance(gameObject.transform.position, points[0]) < 0.001f)
        {
            
            // Swap the position of the cylinder.
            step *= -1.0f;
            dir *= -1;
            // print("Change: "+dir);
        }
    }
    
}
