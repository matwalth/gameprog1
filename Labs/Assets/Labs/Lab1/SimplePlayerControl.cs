using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class SimplePlayerControl : MonoBehaviour
{
    [SerializeField]
    private float speed = 1.0f;
    private int counter = 0;
    private Keyboard key;
    // Start is called before the first frame update
    void Start()
    {
        key = Keyboard.current;
    }

    // Update is called once per frame
    void Update()
    {
        counter++;


        if(key.wKey.isPressed)
        {
            gameObject.transform.Translate(0,speed * Time.deltaTime,0);
        }
        if(key.sKey.isPressed)
        {
            gameObject.transform.Translate(0,-speed * Time.deltaTime,0);
        }
        if(key.aKey.isPressed)
        {
            gameObject.transform.Translate(-speed * Time.deltaTime,0,0);
        }
        if(key.dKey.isPressed)
        {
            gameObject.transform.Translate(speed * Time.deltaTime,0,0);
        }

        print(counter);
    }
}
