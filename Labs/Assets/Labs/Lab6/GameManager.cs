using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    public enum GameState
    {
        Playing, Paused
    }

    //This is a property with default getters and setters
    public GameState CurrentGameState { get; private set; }
    // Events for the game being paused
    public UnityEvent OnGamePaused;
    public UnityEvent OnGameResumed;

    private void Awake()
    {
        // Is this the first time we created this singleton
        if (_instance == null)
        {
            //If we are the first game manager then assign ourselves this instance
            _instance = this;
            //Keep ourselves around between levels
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            //Another game manager exist then destroy this one
            Destroy(this.gameObject);
        }
    }

    // Resumes the game play
    public void ResumeGame()
    {
        //Set the current Game state to playing
        CurrentGameState = GameState.Playing;

        Time.timeScale = 1f;
        //Notify everyone that is listening that game has resumed
        OnGameResumed.Invoke();
    }
    public void TogglePause()
    {
        if(CurrentGameState == GameState.Paused)
        {
            ResumeGame();
        }
        else if (CurrentGameState == GameState.Playing)
        {
            PauseGame();
        }
    }
    public void PauseGame()
    {
        //Set current game state to reflect being paused
        CurrentGameState = GameState.Paused;
        //Adjust the time scale
        Time.timeScale = 0f;
        //Invoke our pause event
        OnGamePaused.Invoke();
    }
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //Resume the game
        ResumeGame();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
