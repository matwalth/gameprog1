using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformerCamera : MonoBehaviour
{
    [Tooltip("Offset from the player we'll follow at (based on the camera's position and the player's position)")]
    [SerializeField] private Vector3 offset;

    [Tooltip("Character we are following")]
    [SerializeField] private GameObject following;
    [SerializeField] private Camera mainCamera;
    
    [SerializeField] private bool isLeftBounded;
    [SerializeField] private bool isRightBounded;
    [SerializeField] private float leftBound;
    [SerializeField] private float rightBound;
    [SerializeField] private float upBound;
    [SerializeField] private float downBound;

    private float adjustedLeftBound;
    private float adjustedRightBound;
    private float adjustedUpBound;
    private float adjustedDownBound;
    private void Awake()
    {
        mainCamera = GetComponent<Camera>();
        if (following == null)
        {
            Debug.LogError("You didn't assign something to follow!");
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        if (following == null)
        {
            Debug.LogError("You didn't assign something to follow!");
        }
        transform.position = new Vector3(following.transform.position.x + offset.x,
        following.transform.position.y + offset.y, transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        calculateBounds();
        //transform.position = new Vector3(following.transform.position.x + offset.x, following.transform.position.y + offset.y, transform.position.z);
        CheckPositionBounds(adjustedLeftBound, adjustedRightBound, adjustedDownBound, adjustedUpBound);
    }

    private void calculateBounds()
    {
        // assume that these are fields in your class, and camera is a reference to the camera
        // we are pointing at--then adjustedLeftBound is the designer's bound + the half-width
        // of the camera. This makes it easier to just set the position of the camera based on
        // its center.
        float bo = (mainCamera.orthographicSize * mainCamera.aspect);
        adjustedLeftBound = leftBound + bo;
        adjustedRightBound = rightBound - bo;
        adjustedDownBound = downBound + bo;
        adjustedUpBound = upBound - bo;
    }

    protected void CheckPositionBounds(float left, float right, float up, float down)
    {
        // take the bigger of these--if the player has moved further
        // left than the left bound (i.e., <) then we want to stick at left
        float leftBound = (following.transform.position.x <= left) ? left : following.transform.position.x + offset.x;

        // now take the smaller of these--if the player has moved
        // beyond the right than the right bound (i.e. >) then we want to stick at the right
        float rightBound = (following.transform.position.x >= right) ? right : following.transform.position.x + offset.x;

        float upBound = (following.transform.position.y >= up) ? up : following.transform.position.y + offset.y;
        float downBound = (following.transform.position.y <= down) ? down : following.transform.position.y + offset.y;

        // now clamp the position between these two values
        float xbounded = Mathf.Clamp(following.transform.position.x, leftBound, rightBound);
        float ybounded = Mathf.Clamp(following.transform.position.y, downBound, upBound);
        transform.position = new Vector3(xbounded, ybounded, transform.position.z);
        return;
    }
}
