using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateController : MonoBehaviour
{
    [Header("State Control")]
    [Tooltip("State that our context is in currently")]
    public State currentState;
    [Tooltip("Whether or not this FSM is active")]
    public bool isActive;

    //Generally shared state goes here

    [Header("Blackboard Variables")]
    [Tooltip("NavMeshAgent associated with our entity")]
    public NavMeshAgent navMesAgent;
    public Animator enemyAnimator;
    //Getting home point
    [Tooltip("The home way point")]
    public Transform homeWayPoint;
    public float waitTime;
    public int patrolIndex = 0;
    [Tooltip("The points that the AI will patrol to")]
    public List<Transform> patrolPoints;
    //Look Decisions
    [Tooltip("Eyes that we're looking from")]
    public Transform AIeyes;
    [Tooltip("Maximum radius we can see")]
    public float lookRadius;
    [Tooltip("Maximum distance we can see")]
    public float lookRange;
    [Tooltip("Maximum radius we can reach to attack")]
    public float attackRadius;
    [Tooltip("Maximum distance we can reach to attack")]
    public float attackRange;
    [Tooltip("Object position we are chasing")]
    public Transform chaseTarget;
    [Tooltip("Object we are attacking")]
    public GameObject attackTarget;
    [Tooltip("Debug Draw color in the game")]
    [SerializeField] private Color lookGizmoColor;
    [SerializeField] private Color attackGizmoColor;
    public float time;
    public float min = 5;
    public float max = 30;
    public float randTime;

    private void Awake()
    {
        //Call our setup to make sure everything is ready
        Setup();
    }
    // Start is called before the first frame update
    void Start()
    {
        randTime = Random.Range(min, max);
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            //update our current state
            currentState.UpdateState(this);
        }
        time = Time.time;


        //Debug.Log(currentState.ToString());
        waitTime += Time.deltaTime;
        
        
    }
    public void Setup()
    {
        //Set up things here

    }
    public void TransitionToState(State nextState)
    {
        // Only transition if its a new state
        if(nextState != null)
        {
            //Call the exit state function
            OnExitState();

            //Transition to the new state
            currentState = nextState;

            //Call the enter state
            currentState.EnterState(this);
        }
    }
    private void OnExitState()
    {
        waitTime = 0;
        currentState.ExitState(this);
    }

    //Draw gizmos related to this AI
    public void OnDrawGizmos()
    {
        //Draw the look location
        Gizmos.color = lookGizmoColor;

        Gizmos.DrawWireSphere(AIeyes.transform.position+ new Vector3(0,1,0) + AIeyes.transform.forward * lookRange, lookRadius);
        Gizmos.color = attackGizmoColor;

        Gizmos.DrawWireSphere(AIeyes.transform.position + new Vector3(0, 1, 0) + AIeyes.transform.forward * attackRange, attackRadius);

    }
}
