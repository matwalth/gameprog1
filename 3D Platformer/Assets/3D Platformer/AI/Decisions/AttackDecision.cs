using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/Attack", fileName = "Attack Decision")]
public class AttackDecision : Decision
{
    [SerializeField] private LayerMask characterLayerMask;
    [SerializeField] private float timeToWait = 1;
    public override bool Decide(StateController controller)
    { 
        bool targetVisible = Attack(controller);
        return targetVisible;
    }
    private bool Attack(StateController controller)
    {
        RaycastHit hit;
       // Collider[] cols;
        if (Physics.SphereCast(controller.AIeyes.position + new Vector3(0,1,0), controller.attackRadius, controller.AIeyes.forward, out hit, controller.attackRange, characterLayerMask, QueryTriggerInteraction.Ignore))
        {
            //GameManager._instance.Attack(); //killing instantly (3 lives)
            //controller.attackTarget.GetComponent<PlayerController>().Kill(); //killing instantly (3 lives)
            controller.enemyAnimator.SetTrigger("Attack");
            controller.chaseTarget = hit.transform;
            return true;
        }
        return false;
    }
}
