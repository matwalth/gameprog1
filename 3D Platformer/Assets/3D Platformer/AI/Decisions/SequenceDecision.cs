using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/SequenceDecision", fileName = "Sequence Decision")]
public class SequenceDecision : Decision
{
    [SerializeField] private List<Decision> decisions;

    public override bool Decide(StateController controller)
    {
        //bool targetVisible = Look(controller);
        for(int i = 0; i<decisions.Count; i++)
        {
            if (!decisions[i].Decide(controller))
            {
                return false;
            }
        }
        return true;
    }
}
