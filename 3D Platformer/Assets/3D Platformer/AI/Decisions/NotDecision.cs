using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/NotDecision", fileName = "Not Decision")]
public class NotDecision : Decision
{
    [SerializeField] private Decision decision;

    public override bool Decide(StateController controller)
    {
        //bool targetVisible = Look(controller);
        return !decision.Decide(controller);
    }
}
