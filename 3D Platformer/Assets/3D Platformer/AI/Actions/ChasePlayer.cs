using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/ChasePlayer", fileName = "Chase Player")]
public class ChasePlayer : Action
{
    public override void Act(StateController controller)
    {
        //Call our interal go home function
        Chase(controller);
    }
    private void Chase(StateController controller)
    {
        //Set our next destination to be our home waypoint
        controller.navMesAgent.destination = controller.chaseTarget.position;
        controller.navMesAgent.isStopped = false;
    }
}
