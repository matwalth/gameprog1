using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Wait", fileName = "Wait Action")]
public class WaitAction : Action
{
    public override void Act(StateController controller)
    {
        //Call our interal go to patrol point function
        Wait(controller);
    }
    //Does nothing just plays idle animation
    private void Wait(StateController controller)
    {

        //if (controller.navMesAgent.remainingDistance < 0.05f)
        //{
        //    Debug.Log("isStopped");
        //    if (controller.patrolIndex + 1 >= controller.patrolPoints.Count)
        //    {
        //        controller.patrolIndex = 0;
        //    }
        //    else
        //    {
        //        controller.patrolIndex++;
        //    }
        //    new WaitForSeconds(5);
        //    Debug.Log("Waiting");
        //}
        controller.navMesAgent.isStopped = true;
    }
    
}
