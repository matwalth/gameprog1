using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Patrol", fileName = "Patrol Action")]
public class PatrolAction : Action
{
    public override void Act(StateController controller)
    {
        //Call our interal go to patrol point function
        GoToPoint(controller);
    }
    private void GoToPoint(StateController controller)
    {
        //Debug.Log("Moving to: "+controller.patrolIndex);
        //Set our next destination to be a random patrol point from the Controller list
        controller.navMesAgent.destination = controller.patrolPoints[controller.patrolIndex].position;
        controller.navMesAgent.isStopped = false;

        /*if (controller.navMesAgent.remainingDistance < 0.05f)
        {
            Debug.Log("Arrived");
            if (controller.patrolIndex + 1 >= controller.patrolPoints.Count)
            {
                controller.patrolIndex = 0;
            }
            else
            {
                controller.patrolIndex++;
            }
            Debug.Log("Waiting");
            new WaitForSeconds(5);
        }*/

        
    }
}
