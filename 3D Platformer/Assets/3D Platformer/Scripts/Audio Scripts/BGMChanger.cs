using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class BGMChanger : MonoBehaviour
{
    [SerializeField] private AudioClip victorySFX;
    [SerializeField] private AudioClip defeatSFX;
    [SerializeField] private AudioClip BGM;
    [SerializeField] private AudioSource audioSource;

    private void Start()
    {
        GameManager._instance.OnGameStart.AddListener(StartMusic);
    }

    private void Update()
    {
        //Play audio associated with key press
        
    }
    private void PlaySFX(AudioClip clipToPlay)
    {
        //If clipToPlay is already playing then do nothing
        //if (clipToPlay == audioSource.clip) return;
        //Stop currently playing sound if any
        audioSource.Stop();
        //Set what clip to play
        audioSource.clip = clipToPlay;
        //Play the sound
        audioSource.Play();
    }
    public void Victory()
    {
        PlaySFX(victorySFX);
    }
    public void Defeat()
    {
        PlaySFX(defeatSFX);
    }
    public void StartMusic()
    {
        PlaySFX(BGM);
    }

}
