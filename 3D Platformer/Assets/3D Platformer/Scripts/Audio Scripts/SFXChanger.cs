using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class SFXChanger : MonoBehaviour
{
    //[SerializeField] private GameObject LouderSFX;
    [SerializeField] private AudioClip diedSFX;
    [SerializeField] private AudioClip hurtSFX;
    [SerializeField] private AudioClip runSFX;
    [SerializeField] private AudioClip jumpSFX;
    [SerializeField] private AudioClip landsSFX;
    [SerializeField] private AudioSource audioSource;
    private bool running = false;
    private bool jumping = false;

    private void Update()
    {
        
        
    }
    private void PlaySFX(AudioClip clipToPlay)
    {
        //If clipToPlay is already playing running while currently running then do nothing
        if (clipToPlay == audioSource.clip && running) return;
        //Stop currently playing sound if any
        //audioSource.Stop();
        //Set what clip to play
        audioSource.clip = clipToPlay;
        //Play the sound
        audioSource.Play();
        //Debug.Log("Playing SFX");
    }
    
/*    public void Death()
    {
        PlaySFX(diedSFX);
    }*/
/*    public void Hurt()
    {
        Debug.Log("Hurt SFX");
        PlaySFX(hurtSFX);
    }*/
    public void Run()
    {
        if (jumping == false)
        {
            //Debug.Log("Running");
            PlaySFX(runSFX);
            running = true;
            audioSource.loop = true;
        }
    }
    public void StopSFX()
    {
        if (audioSource.clip == runSFX) { 
            //Debug.Log("Stop SFX");
            audioSource.Stop();
            running = false;
            audioSource.loop = false;
        }
    }
    public void Jump()
    {
        //Debug.Log("Jump SFX");
        PlaySFX(jumpSFX);
        running = false;
        audioSource.loop = false;
        //jumping = true;
    }
    public void Land()
    {
        jumping = false;
        running = false;
    }
/*    public void Land()
    {
        PlaySFX(landsSFX);
        running = false;
        audioSource.loop = false;
    }*/
}
