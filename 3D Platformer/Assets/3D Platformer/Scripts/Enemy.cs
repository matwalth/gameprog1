using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    [Header("Enemy Collider")]
    //[SerializeField] private Rigidbody enemyRigidbody;
    [SerializeField] private SphereCollider sphere;
    [SerializeField] private Animator animator;
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().Kill();
            animator.SetTrigger("Attack");
        }
    }
}
