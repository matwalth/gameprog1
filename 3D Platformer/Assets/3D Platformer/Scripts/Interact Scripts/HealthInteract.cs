using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthInteract : MonoBehaviour, IInteractable
{
    [SerializeField] GameObject thisGameObject;
    public void Interact(PlayerInteractManager pim, PlayerController pc)
    {
        //Tell interactBehavior ScriptObject to do its Interact functionality
        GameManager._instance.GainHealth();
        //set this object inactive
        thisGameObject.SetActive(false);

        pim.UnTrackObject(thisGameObject);
    }
}
