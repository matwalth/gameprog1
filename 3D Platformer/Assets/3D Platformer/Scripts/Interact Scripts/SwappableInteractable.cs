using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwappableInteractable : MonoBehaviour, IInteractable
{
    enum InteractAction
    {
        Sound, Print, Move
    }
    [Header("Current Action")]
    [SerializeField] private InteractAction interactAction = InteractAction.Sound;

    [Header("Print Message")]
    [SerializeField] private string messageToPrint;

    [Header("Play Sound")]
    [SerializeField] private AudioClip clipToPlay;
    [SerializeField] private AudioSource audioSource;

    [Header("Distance to Move")]
    [SerializeField] private Vector3 movementAmount;
    public void Interact(PlayerInteractManager pim, PlayerController pc)
    {
        if(interactAction == InteractAction.Sound)
        {
            audioSource.Stop();
            audioSource.clip = clipToPlay;
            audioSource.Play();
        }
        if(interactAction == InteractAction.Print)
        {
            Debug.Log(messageToPrint);
        }
        if(interactAction == InteractAction.Move)
        {
            transform.position = transform.position + movementAmount;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
