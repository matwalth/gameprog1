using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
public class StartMenuController : MonoBehaviour
{
    [SerializeField] private UIDocument UIDoc;
    private VisualElement menu;
    private bool creditStatus = false;
    private Button quit;
    private Button start;
    private Button credits;
    [SerializeField] GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        GameManager._instance.OnGameStart.AddListener(Quit);
        Time.timeScale = 0;
        VisualElement root = UIDoc.rootVisualElement;
        player.GetComponent<PlayerController>().StartScreen();
        menu = root.Q<VisualElement>("credit-menu");
        start = root.Q<Button>("start-button");
        credits = root.Q<Button>("credits-button");
        quit = root.Q<Button>("quit-button");
        menu.style.visibility = Visibility.Hidden;
        quit.clicked += ButtonFunction;
        start.clicked += StartClicked;
        credits.clicked += OnCreditClicked;
    }
    private void OnDestroy()
    {
        quit.clicked -= ButtonFunction;
        start.clicked -= StartClicked;
        credits.clicked -= OnCreditClicked;
    }
    private void ButtonFunction()
    {
        Debug.Log("Button was Pressed");
    }
    private void StartClicked()
    {
        //player.GetComponent<Lara_Controls>().GameStart();
        GameManager._instance.GameStart();
        Time.timeScale = 1;
        UIDoc.rootVisualElement.style.visibility = Visibility.Hidden;
    }
    public void GameOver()
    {
        Debug.Log("Game Over");
        UnityEngine.Cursor.visible = true;
        UnityEngine.Cursor.lockState = CursorLockMode.Confined;
        UIDoc.rootVisualElement.style.visibility = Visibility.Visible;
    }
    public void Quit()
    {
        Debug.Log("Quitting to Main Menu");
        UnityEngine.Cursor.visible = true;
        UnityEngine.Cursor.lockState = CursorLockMode.Confined;
        UIDoc.rootVisualElement.style.visibility = Visibility.Visible;
    }
    private void OnCreditClicked()
    {
        //Debug.Log("Credits clicked");
        if (!creditStatus)
        {
            menu.style.visibility = Visibility.Visible;
            creditStatus = true;
        }
        else
        {
            menu.style.visibility = Visibility.Hidden;
            creditStatus = false ;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
