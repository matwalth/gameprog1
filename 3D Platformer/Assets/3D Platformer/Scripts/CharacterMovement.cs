using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : BaseMovement
{
    [Header("Ground Movement")]
    [Tooltip("Speed that ths character accelerates")]
    [SerializeField] private float maxAcceleration = 60f;

    [Tooltip("Current max speed of this character")]
    [SerializeField] private float moveSpeed = 7f;
    
    [SerializeField] private Vector3 cameraAdjustedInputDirection;

    [Tooltip("Rigidbody of this character")]
    [SerializeField] private Rigidbody characterRigidbody;

    [Tooltip("Speed which we rotate at")]
    [SerializeField] private float rotationSpeed;

    [Header("Slope Handling")]
    [Tooltip("The maximum angle of ground we still consider to be a slope")]
    [SerializeField] private float maxSlopeAngle = 44f;
    [Tooltip("The amount of force used to keep the character from flying off a slope when moveing")]
    [SerializeField] private float slopeStickingForce = 2f;
    [Tooltip("Stores information about the slope we are standing on")]
    RaycastHit slopeHit;
    [Tooltip("Denotes whether you are on a slope or not")]
    [SerializeField] private bool isOnSlope;
    [Tooltip("A boolean flag that allows us to jump or walk off of a slope")]
    private bool exitingSlope = false;


    [Header("Ground Check")]
    [Tooltip("Distance below the player to check for the ground")]
    [SerializeField] private float groundCheckDistance = .1f;

    [Tooltip("Which layers are considered ground")]
    [SerializeField] private LayerMask environmentLayerMask;

    [Tooltip("Whether we are grounded or not")]
    [SerializeField] private bool isGrounded;

    [Header("Air Movement")]
    [Tooltip("Maximum speed we can move in the air")]
    [SerializeField] private float maxVerticalMoveSpeed = 25f;
    [SerializeField] private float jumpForce;

    [Header("Object References")]
    [Tooltip("The capsule collider on this object")]
    [SerializeField] CapsuleCollider capsuleCollider;

    [Tooltip("Character mesh")]
    [SerializeField] private Transform characterModel;

    [SerializeField] private Animator characterAnimator;
    [SerializeField] private AudioSource SFX;
    [SerializeField] private AudioSource BGM;
    [SerializeField] private AudioSource loudSFX;
    [Header("Cameras")]
    [SerializeField] private GameObject[] cams = new GameObject[4];
    //Flag to indicate whethere we were grounded last frame
    private bool wasGroundedLastFrame;

    //Function to check wherther we are gorunded and to set our isGrounded flag
    private void CheckSlope()
    {
        // If we are not on the ground, we can't be on a slope,
        // so there's no need to perform any operations
        if (!isGrounded)
        {
            isOnSlope = false;
        }
        // Raycast downwards to find the ground, then check if the
        // angle of the slope is within our maxSlopeAngle and not 0.
        // If we wanted to be extra thorough, we should probably perform
        // a raycast at several points around the edge of our character's
        // collider, but that's a little overkill for our purposes.
        else if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out slopeHit, groundCheckDistance + 0.1f))
        {
            float angle = Vector3.Angle(Vector3.up, slopeHit.normal);
            isOnSlope = ((angle < maxSlopeAngle) && (angle != 0f));
        }
        else
        {
            // We are either not on a slope, or we are on something
            // too steep to be counted as a slope.
            isOnSlope = false;
        }
    }
    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireCube()
    //}
    private void CheckGround()
    {
        //Store isGrounded from the last frame
        wasGroundedLastFrame = isGrounded;

        //Use an overlapping sphere check to see if we are grounded
        Vector3 origin = transform.position + (Vector3.up * (capsuleCollider.radius - groundCheckDistance));

        //Query the physics engine for collisions, ignore triggers, only use out environmentLayerMask
        Collider[] overlappedColliders = Physics.OverlapSphere(origin, capsuleCollider.radius * .95f, environmentLayerMask, QueryTriggerInteraction.Ignore);

        //Determine if we are grounded
        isGrounded = overlappedColliders.Length > 0;

        //Set our animator parameter here!
        characterAnimator.SetBool("IsGrounded", isGrounded);
        
    }
    //Called by FixedUpdate()
    private void MoveCharacter()
    {
        //If they're moving the controller, then move the camera
        if(cameraAdjustedInputDirection != Vector3.zero)
        {
            // Turn off gravity while on slope.
            // This will prevent our character
            // from sliding down the slope
            // due to gravity/friction.
            //characterRigidbody.useGravity = !isOnSlope;

            // If we are on a slope, we want to make sure we align
            // our movement input with whatever slope we're standing
            // on so that we don't get stuck on the slope when moving
            // towards it and we don't fly off the slope when moving
            // away from it.
            if (isOnSlope)
            {
                // Apply our movement force, adjusted
                // to be aligned with the slope we're on.
                Vector3 slopeAdjustedMoveDirection = Vector3.ProjectOnPlane(cameraAdjustedInputDirection, slopeHit.normal).normalized;
                Vector3 accel = slopeAdjustedMoveDirection * maxAcceleration;
                //willCollide = WillCollide(accel);
                characterRigidbody.AddForce(accel * characterRigidbody.mass, ForceMode.Force);

                // If we're moving up or down the slope, add a
                // downwards force to keep our character stuck
                // to the platform so we don't fly off when moving.
                if (!Mathf.Approximately(characterRigidbody.velocity.y, 0f))
                {
                    characterRigidbody.AddForce(Vector3.down * slopeStickingForce * characterRigidbody.mass, ForceMode.Force);
                }
            }
            //Apply a force to the character to move them
            characterRigidbody.AddForce(cameraAdjustedInputDirection * maxAcceleration * characterRigidbody.mass, ForceMode.Force);
            SFX.GetComponent<SFXChanger>().Run();
        }
        characterAnimator.SetFloat("MoveSpeed", GetHorizontalRBVelocity().magnitude);
        if (GetHorizontalRBVelocity().magnitude < 0.05f)
        {
            SFX.GetComponent<SFXChanger>().StopSFX();
        }
    }

    //A function to return the maximum velocity we can travel
    private float GetMaxAllowedVelocity()
    {
        return moveSpeed * cameraAdjustedInputDirection.magnitude;
    }
    private float GetMaxAllowedVerticalVelocity()
    {
        return maxVerticalMoveSpeed * cameraAdjustedInputDirection.magnitude;
    }
    //Return the rigid body veocity of this character
    private Vector3 GetHorizontalRBVelocity()
    {
        return new Vector3(characterRigidbody.velocity.x, 0, characterRigidbody.velocity.z);
    }
    private Vector3 GetVerticalRBVelocity()
    {
        return new Vector3(0, characterRigidbody.velocity.y, 0);
    }
    //Make sure we never exceed horizontal or vertical vaps on velocity
    private void LimitVelocity()
    {
        //Get our horizontal velocity
        Vector3 currentVelocity = GetHorizontalRBVelocity();

        //Get our max velocity
        float maxAllowedVelocity = GetMaxAllowedVelocity();

        if(currentVelocity.sqrMagnitude > (maxAllowedVelocity * maxAllowedVelocity))
        {
            //Use an impulse force to counteract the speed of this player
            Vector3 counteractDirection = currentVelocity.normalized * -1f;
            float counteractAmount = currentVelocity.magnitude - maxAllowedVelocity;
            characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
        }

        //Now check for falling/jumping
        if (!isGrounded)
        {
            if(Mathf.Abs(characterRigidbody.velocity.y) > maxVerticalMoveSpeed)
            {
                //Use an impulse to counteract the speed
                Vector3 counteractDirection = Vector3.up * Mathf.Sign(characterRigidbody.velocity.y) * -1f;

                float counteractAmount = Mathf.Abs(characterRigidbody.velocity.y) - maxVerticalMoveSpeed;

                characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
            }
        }
    }
    #region BaseMovement Functions
    override public void Move(Vector3 moveDir)
    {
        //Implement movement
        cameraAdjustedInputDirection = moveDir;

    }

    override public void RotateCharacter()
    {
        //Make sure we have player input
        if(cameraAdjustedInputDirection != Vector3.zero)
        {
            characterModel.forward = Vector3.Slerp(characterModel.forward, cameraAdjustedInputDirection.normalized, Time.deltaTime * rotationSpeed);
        }
    }
    override public void Jump()
    {
      
        
            if (isGrounded)
            {
                characterRigidbody.AddForce(Vector3.up * jumpForce * characterRigidbody.mass, ForceMode.Impulse);
                characterAnimator.SetTrigger("Jump");
                SFX.GetComponent<SFXChanger>().Jump();
            //characterAnimator.SetBool("Jump", true);
        }
        
    }
    override public void JumpCanceled()
    {
        if (characterRigidbody.velocity.y > 0)
        {
            characterRigidbody.AddForce(Vector3.down * characterRigidbody.velocity.y * .5f * characterRigidbody.mass, ForceMode.Impulse);
            characterAnimator.SetTrigger("Fall");
            if (isGrounded)
            {
                loudSFX.GetComponent<LouderSFX>().Land();
            }
        }
        //characterAnimator.SetBool("Jump", false);
    }

    #endregion

    #region Unity Functions
    // Start is called before the first frame update
    override protected void Start()
    {
        
    }

    // Update is called once per frame
    override protected void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            cams[0].SetActive(true);
            cams[1].SetActive(false);
            cams[2].SetActive(false);
            cams[3].SetActive(false);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            cams[1].SetActive(true);
            cams[0].SetActive(false);
            cams[2].SetActive(false);
            cams[3].SetActive(false);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            cams[2].SetActive(true);
            cams[0].SetActive(false);
            cams[1].SetActive(false);
            cams[3].SetActive(false);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            cams[3].SetActive(true);
            cams[0].SetActive(false);
            cams[1].SetActive(false);
            cams[2].SetActive(false);
        }
    }
    override protected void FixedUpdate()
    {
        //Check to see if we are grounded
        CheckGround();
        CheckSlope();
        MoveCharacter();

        LimitVelocity();

        RotateCharacter();

    }
    #endregion
}
