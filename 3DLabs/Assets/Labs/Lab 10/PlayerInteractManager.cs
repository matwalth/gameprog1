using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class PlayerInteractManager : MonoBehaviour
{
    //A list of all objects we can currently interact with
    List<GameObject> interactableObjects = new List<GameObject>();
    [Tooltip("Event called when we go from 0 to at least 1 interactable object in range")]
    public UnityEvent OnInteractablesExist;

    [Tooltip("Event called when we go from some # to 0 interactables in range")]
    public UnityEvent OnInteractablesDoNotExist;

    [Tooltip("Player controller on the player (Drag it here)")]
    [SerializeField] private PlayerController playerController;

    private void TrackObject(GameObject objectToTrack)
    {
        //Add the object to our list of tracked objects
        interactableObjects.Add(objectToTrack);

        //Interactables exist so let everyone know
        if(interactableObjects.Count == 1)
        {
            OnInteractablesExist.Invoke();
        }
    }
    public void UnTrackObject(GameObject trackedObject)
    {
        //Only try to remove if its already present
        if (interactableObjects.Contains(trackedObject))
        {
            interactableObjects.Remove(trackedObject);
            //See if we hit 0 and let folks know
            if(interactableObjects.Count == 0)
            {
                OnInteractablesDoNotExist.Invoke();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //If an interactable enters our trigger area
        if (other.CompareTag("Interactable"))
        {
            //Then track it!
            TrackObject(other.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Interactable"))
        {
            UnTrackObject(other.gameObject);
        }
    }

    public void Interact()
    {
        //Clean up the list first
        while (interactableObjects.Count > 0 && interactableObjects[0] == null)
        {
            UnTrackObject(interactableObjects[0]);
        }
        //Only interact if we have something to do so with
        if (interactableObjects.Count > 0)
        {
            //Interact only with the first one
            interactableObjects[0].GetComponent<IInteractable>().Interact(this, playerController);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
