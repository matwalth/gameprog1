using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Audio/RandomAudioCollection", fileName = "RandomAudioCollection")]
public class RandomAudioCollection : ScriptableObject
{
    [Tooltip("List of all the audio clips to choose from")]
    [SerializeField] private List<AudioClip> listOfClips;

    //Pick a random clip from our list of clips to play in our given audio source
    public void PlayRandomAudioClip(AudioSource audioSource)
    {
        //If there is at least one clip in the list
        if(listOfClips.Count > 0)
        {
            //Pick a random audio clip and set as the audioSource's clip
            audioSource.clip = listOfClips[Random.Range(0, listOfClips.Count)];
            //Tell the audioSource to play the clip
            audioSource.Play();
            
        }
    }
}
