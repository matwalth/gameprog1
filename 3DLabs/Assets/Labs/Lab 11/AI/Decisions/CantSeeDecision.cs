using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/CantSee", fileName = "CantSee Decision")]
public class CantSeeDecision : Decision
{
    [SerializeField] private LayerMask characterLayerMask;

    public override bool Decide(StateController controller)
    {
        bool targetVisible = Look(controller);
        return !targetVisible;
    }
    private bool Look(StateController controller)
    {
        RaycastHit hit;
        // Collider[] cols;
        if (Physics.SphereCast(controller.AIeyes.position, controller.lookRadius, controller.AIeyes.forward, out hit, controller.lookRange, characterLayerMask, QueryTriggerInteraction.Ignore))
        {
            controller.chaseTarget = hit.transform;
            return true;
        }
        return false;
    }
}
