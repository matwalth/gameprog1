using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Transition
{
    [Tooltip("The decision being evaluated by this transition")]
    public Decision decision;

    [Tooltip("The next state we will transition to")]
    public State nextState;


}
