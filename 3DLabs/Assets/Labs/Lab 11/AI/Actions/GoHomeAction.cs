using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="PluggableAI/Actions/GoHome", fileName = "Go Home")]
public class GoHomeAction : Action
{
    public override void Act(StateController controller)
    {
        //Call our interal go home function
        GoHome(controller);
    }
    private void GoHome(StateController controller)
    {
        //Set our next destination to be our home waypoint
        controller.navMesAgent.destination = controller.homeWayPoint.position;
        controller.navMesAgent.isStopped = false;
    }
}
