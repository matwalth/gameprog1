using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/State", fileName = "State")]
public class State : ScriptableObject
{
    [Tooltip("List of actions that occur when entering this state")]
    [SerializeField] private List<Action> enterActions;
    [Tooltip("List of actions that occur when exiting this state")]
    [SerializeField] private List<Action> exitActions;
    [Tooltip("List of actions that occur when updating this state")]
    [SerializeField] private List<Action> updateActions;

    [Tooltip("A list of all possible transitions from this state")]
    [SerializeField] private List<Transition> transitions;

    public void EnterState(StateController controller)
    {
        //Perform entrance actions
        DoEnterActions(controller);
    }
    public void ExitState(StateController controller)
    {
        //Perform exit actions
        DoExitActions(controller);
    }
    public void UpdateState(StateController controller)
    {
        //Perform update actions
        DoUpdateActions(controller);

        //Check transitions
        CheckTransitions(controller);
    }

    private void DoEnterActions(StateController controller)
    {
        //Walk through the actions and execute them
        foreach (Action enterAction in enterActions)
        {
            enterAction.Act(controller);
        }
    }
    private void DoExitActions(StateController controller)
    {
        //Walk through the actions and execute them
        foreach (Action exitAction in exitActions)
        {
            exitAction.Act(controller);
        }
    }
    private void DoUpdateActions(StateController controller)
    {
        //Walk through the actions and execute them
        foreach (Action updateAction in updateActions)
        {
            updateAction.Act(controller);
        }
    }
    private void CheckTransitions(StateController controller)
    {
        //For every transition this state contains
        foreach(Transition transition in transitions)
        {
            //Evaluate the result of the decision
            bool decisionSucceeded = transition.decision.Decide(controller);

            if (decisionSucceeded)
            {
                //Tell the controller to move to the next state
                controller.TransitionToState(transition.nextState);
                //Stop trying to transition
                break;
            }
        }
    }
}
