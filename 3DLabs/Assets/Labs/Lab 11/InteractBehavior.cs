using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Inherits from scriptableObject rather than monoBehaviour
public abstract class InteractBehavior : ScriptableObject
{
    public abstract void Interact();
}
