using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "InteractBehaviors/PrintMessage", fileName = "PrintMessage")]
public class PrintMessageIBSO : InteractBehavior
{
    [Tooltip("Message to print when interacted with")]
    [SerializeField] private string messageToPrint;
    //Interact function defined in InteractBehavior
    public override void Interact()
    {
        Debug.Log(messageToPrint);
    }
}
