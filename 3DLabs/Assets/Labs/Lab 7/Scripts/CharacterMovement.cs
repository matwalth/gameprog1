using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : BaseMovement
{
    [Header("Ground Movement")]
    [Tooltip("Speed that ths character accelerates")]
    [SerializeField] private float maxAcceleration = 60f;

    [Tooltip("Current max speed of this character")]
    [SerializeField] private float moveSpeed = 7f;
    
    [SerializeField] private Vector3 cameraAdjustedInputDirection;

    [Tooltip("Rigidbody of this character")]
    [SerializeField] private Rigidbody characterRigidbody;

    [Tooltip("Speed which we rotate at")]
    [SerializeField] private float rotationSpeed;


    [Header("Ground Check")]
    [Tooltip("Distance below the player to check for the ground")]
    [SerializeField] private float groundCheckDistance = .1f;

    [Tooltip("Which layers are considered ground")]
    [SerializeField] private LayerMask environmentLayerMask;

    [Tooltip("Whether we are grounded or not")]
    [SerializeField] private bool isGrounded;

    [Header("Air Movement")]
    [Tooltip("Maximum speed we can move in the air")]
    [SerializeField] private float maxVerticalMoveSpeed = 25f;
    [SerializeField] private float jumpForce;

    [Header("Object References")]
    [Tooltip("The capsule collider on this object")]
    [SerializeField] CapsuleCollider capsuleCollider;

    [Tooltip("Character mesh")]
    [SerializeField] private Transform characterModel;

    [SerializeField] private Animator characterAnimator;
    //Flag to indicate whethere we were grounded last frame
    private bool wasGroundedLastFrame;

    //Function to check wherther we are gorunded and to set our isGrounded flag
    private void CheckGround()
    {
        //Store isGrounded from the last frame
        wasGroundedLastFrame = isGrounded;

        //Use an overlapping sphere check to see if we are grounded
        Vector3 origin = transform.position + (Vector3.up * (capsuleCollider.radius - groundCheckDistance));

        //Query the physics engine for collisions, ignore triggers, only use out environmentLayerMask
        Collider[] overlappedColliders = Physics.OverlapSphere(origin, capsuleCollider.radius * .95f, environmentLayerMask, QueryTriggerInteraction.Ignore);

        //Determine if we are grounded
        isGrounded = overlappedColliders.Length > 0;

        //Set our animator parameter here!
        characterAnimator.SetBool("IsGrounded", isGrounded);
        
    }
    //Called by FixedUpdate()
    private void MoveCharacter()
    {
        //If they're moving the controller, then move the camera
        if(cameraAdjustedInputDirection != Vector3.zero)
        {
            //Apply a force to the character to move them
            characterRigidbody.AddForce(cameraAdjustedInputDirection * maxAcceleration * characterRigidbody.mass, ForceMode.Force);
        }
        characterAnimator.SetFloat("MoveSpeed", GetHorizontalRBVelocity().magnitude);
    }

    //A function to return the maximum velocity we can travel
    private float GetMaxAllowedVelocity()
    {
        return moveSpeed * cameraAdjustedInputDirection.magnitude;
    }
    private float GetMaxAllowedVerticalVelocity()
    {
        return maxVerticalMoveSpeed * cameraAdjustedInputDirection.magnitude;
    }
    //Return the rigid body veocity of this character
    private Vector3 GetHorizontalRBVelocity()
    {
        return new Vector3(characterRigidbody.velocity.x, 0, characterRigidbody.velocity.z);
    }
    private Vector3 GetVerticalRBVelocity()
    {
        return new Vector3(0, characterRigidbody.velocity.y, 0);
    }
    //Make sure we never exceed horizontal or vertical vaps on velocity
    private void LimitVelocity()
    {
        //Get our horizontal velocity
        Vector3 currentVelocity = GetHorizontalRBVelocity();

        //Get our max velocity
        float maxAllowedVelocity = GetMaxAllowedVelocity();

        if(currentVelocity.sqrMagnitude > (maxAllowedVelocity * maxAllowedVelocity))
        {
            //Use an impulse force to counteract the speed of this player
            Vector3 counteractDirection = currentVelocity.normalized * -1f;
            float counteractAmount = currentVelocity.magnitude - maxAllowedVelocity;
            characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
        }

        //Now check for falling/jumping
        if (!isGrounded)
        {
            if(Mathf.Abs(characterRigidbody.velocity.y) > maxVerticalMoveSpeed)
            {
                //Use an impulse to counteract the speed
                Vector3 counteractDirection = Vector3.up * Mathf.Sign(characterRigidbody.velocity.y) * -1f;

                float counteractAmount = Mathf.Abs(characterRigidbody.velocity.y) - maxVerticalMoveSpeed;

                characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
            }
        }
    }
    #region BaseMovement Functions
    override public void Move(Vector3 moveDir)
    {
        //Implement movement
        cameraAdjustedInputDirection = moveDir;

    }

    override public void RotateCharacter()
    {
        //Make sure we have player input
        if(cameraAdjustedInputDirection != Vector3.zero)
        {
            characterModel.forward = Vector3.Slerp(characterModel.forward, cameraAdjustedInputDirection.normalized, Time.deltaTime * rotationSpeed);
        }
    }
    override public void Jump()
    {
      
        
            if (isGrounded)
            {
                characterRigidbody.AddForce(Vector3.up * jumpForce * characterRigidbody.mass, ForceMode.Impulse);
                characterAnimator.SetTrigger("Jump");
                //characterAnimator.SetBool("Jump", true);
            }
        
    }
    override public void JumpCanceled()
    {
        if (characterRigidbody.velocity.y > 0)
        {
            characterRigidbody.AddForce(Vector3.down * characterRigidbody.velocity.y * .5f * characterRigidbody.mass, ForceMode.Impulse);
            characterAnimator.SetTrigger("Fall");
        }
        //characterAnimator.SetBool("Jump", false);
    }
    #endregion
    #region Unity Functions
    // Start is called before the first frame update
    override protected void Start()
    {
        
    }

    // Update is called once per frame
    override protected void Update()
    {
        
    }
    override protected void FixedUpdate()
    {
        //Check to see if we are grounded
        CheckGround();

        MoveCharacter();

        LimitVelocity();

        RotateCharacter();

    }
    #endregion
}
