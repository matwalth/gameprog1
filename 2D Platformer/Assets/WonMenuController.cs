using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class WonMenuController : MonoBehaviour
{

    [SerializeField] private UIDocument UIDoc;
    private Button quit;
    [SerializeField] GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        GameManager._instance.OnGameWon.AddListener(PlayerWon);
        GameManager._instance.OnGameStart.AddListener(Restart);
        UIDoc.rootVisualElement.style.visibility = Visibility.Hidden;
        VisualElement root = UIDoc.rootVisualElement;
        quit = root.Q<Button>("won-quit");
        quit.clicked += ButtonFunction;
    }
    public void Restart()
    {
        UIDoc.rootVisualElement.style.visibility = Visibility.Hidden;
    }
    private void OnDestroy()
    {
        quit.clicked -= ButtonFunction;
    }
    private void ButtonFunction()
    {
        GameManager._instance.QuitGame();
        //player.GetComponent<Lara_Controls>().QuitGameFromLose();
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void PlayerWon()
    {
        UIDoc.rootVisualElement.style.visibility = Visibility.Visible;
        UnityEngine.Cursor.visible = true;
        UnityEngine.Cursor.lockState = CursorLockMode.Confined;
    }
}
