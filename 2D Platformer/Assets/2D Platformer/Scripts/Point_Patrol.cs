using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point_Patrol : MonoBehaviour
{

    [Header("Enemy Movement")]
    [SerializeField] private float moveSpeed = 0.8f;
    //[SerializeField] private float patrolTime = 2;
    [SerializeField] private List<Vector2> horizontalPoints = new List<Vector2>() { new Vector2(1, 0), new Vector2(3, 0) };
    [SerializeField] private List<Vector2> verticalPoints = new List<Vector2>() { new Vector2(0, 4), new Vector2(0, 0) };
    [SerializeField] private int pointDistance = 2;
    [SerializeField] private int verticalDistance = 2;
    private int index = 0;

    private enum Direction
    {
        Right, Left, Up, Down
    }
    private Direction moveDirection = Direction.Left;
    private Direction climbDirection = Direction.Up;
    [SerializeField] private bool enemyPatrolling = true;
    [SerializeField] private bool enemyClimbing = true;
    [Header("Enemy Animation")]
    [SerializeField] private Rigidbody2D enemyRigidbody;
    [SerializeField] private BoxCollider2D boxCollider2D;
    [SerializeField] private SpriteRenderer spriteRenderer;


    private void Move()
    {
        Vector2 targetPosition = transform.position;

        switch (moveDirection)
        {
            default:
                //targetPosition += (points[index] - targetPosition) * moveSpeed * Time.fixedDeltaTime;
                //break;
            case Direction.Right:
                targetPosition += Vector2.right * moveSpeed * Time.fixedDeltaTime;
                break;
            case Direction.Left:
                targetPosition -= Vector2.right * moveSpeed * Time.fixedDeltaTime;
                break;
            case Direction.Up:
                targetPosition += Vector2.up * moveSpeed * Time.fixedDeltaTime;
                break;
            case Direction.Down:
                targetPosition += Vector2.down * moveSpeed * Time.fixedDeltaTime;
                break;
        }

        enemyRigidbody.MovePosition(targetPosition);
    }
    private void Climb()
    {
        Vector2 targetPosition = transform.position;

        switch (climbDirection)
        {
            default:
            //targetPosition += (points[index] - targetPosition) * moveSpeed * Time.fixedDeltaTime;
            //break;
            case Direction.Up:
                targetPosition += Vector2.up * moveSpeed * Time.fixedDeltaTime;
                break;
            case Direction.Down:
                targetPosition += Vector2.down * moveSpeed * Time.fixedDeltaTime;
                break;
        }

        enemyRigidbody.MovePosition(targetPosition);
    }
    private void FlipMoveDirection()
    {
        if (moveDirection == Direction.Right)
        {
            moveDirection = Direction.Left;
            spriteRenderer.flipX = false;
        }
        else
        {
            moveDirection = Direction.Right;
            spriteRenderer.flipX = true;
        }
    }
    private void FlipClimbDirection()
    {
        Debug.Log(climbDirection);
        if (climbDirection == Direction.Up)
        {
            Debug.Log("Climbing Down");
            climbDirection = Direction.Down;
        }
        else
        {
            Debug.Log("Climbing Up");
            climbDirection = Direction.Up;
        }
    }
    private IEnumerator EnemyPatrol()
    {
        while (enemyPatrolling)
        {
            int length = horizontalPoints.Count;
            for (int i = 0; i < length; i++)
            {
                index = i;
                //horizontalPoints = new List<Vector2>() { new Vector2(transform.position.x - pointDistance, transform.position.y), new Vector2(transform.position.x + pointDistance, transform.position.y) };
                while (Vector2.Distance(transform.position, horizontalPoints[i]) > 0.001f)
                {
                    //Debug.Log("Almost at point "+i);
                    // Move the enemy
                    Move();

                    yield return new WaitForFixedUpdate();
                }
                //Debug.Log("Arrived at point " + i);
                yield return new WaitForSeconds(5);
                if (enemyClimbing)
                {
                    verticalPoints = new List<Vector2>() { new Vector2(transform.position.x, transform.position.y + verticalDistance), new Vector2(transform.position.x, transform.position.y) };
                    for (int v = 0; v < verticalPoints.Count; v++)
                    {

                        //Debug.Log("Climbing "+ verticalPoints.Count);
                        while (Vector2.Distance(transform.position, verticalPoints[v]) > 0.001f)
                        {
                            //Debug.Log("Almost at point "+i);
                            // Move the enemy
                            Climb();

                            yield return new WaitForFixedUpdate();
                        }
                        //Debug.Log("Arrived at point " + i);
                        yield return new WaitForSeconds(5);
                        FlipClimbDirection();
                    }
                }
                    // Flip after reaching patrol point
                    FlipMoveDirection();
            }
            
        }
        while (enemyClimbing)
        {
            verticalPoints = new List<Vector2>() { new Vector2(transform.position.x, transform.position.y + verticalDistance), new Vector2(transform.position.x, transform.position.y) };
            for (int v = 0; v < verticalPoints.Count; v++)
            {

                //Debug.Log("Climbing "+ verticalPoints.Count);
                while (Vector2.Distance(transform.position, verticalPoints[v]) > 0.001f)
                {
                    //Debug.Log("Almost at point "+i);
                    // Move the enemy
                    Climb();

                    yield return new WaitForFixedUpdate();
                }
                //Debug.Log("Arrived at point " + i);
                yield return new WaitForSeconds(5);
                FlipClimbDirection();
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        horizontalPoints = new List<Vector2>() { new Vector2(transform.position.x - pointDistance, transform.position.y), new Vector2(transform.position.x + pointDistance, transform.position.y) };
        verticalPoints = new List<Vector2>() { new Vector2(transform.position.x, transform.position.y + verticalDistance), new Vector2(transform.position.x, transform.position.y) };
        // Start the Coroutine for the enemy 
        StartCoroutine(EnemyPatrol());
    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            // other.get
        }
    }
}
