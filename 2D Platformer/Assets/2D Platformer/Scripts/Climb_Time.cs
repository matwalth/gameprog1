using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Climb_Time : MonoBehaviour
{

    [Header("Enemy Movement")]
    [SerializeField] private float moveSpeed = 0.8f;
    [SerializeField] private float patrolTime = 2;


    private enum Direction
    {
        Up, Down
    }
    private Direction moveDirection = Direction.Up;
    [SerializeField] private bool enemyPatrolling = true;

    [Header("Enemy Animation")]
    [SerializeField] private Rigidbody2D enemyRigidbody;
    [SerializeField] private BoxCollider2D boxCollider2D;
    [SerializeField] private SpriteRenderer spriteRenderer;


    private void Move()
    {
        Vector2 targetPosition = transform.position;

        switch (moveDirection)
        {
            default:
            //targetPosition += (points[index] - targetPosition) * moveSpeed * Time.fixedDeltaTime;
            //break;
            case Direction.Up:
                targetPosition += Vector2.up * moveSpeed * Time.fixedDeltaTime;
                break;
            case Direction.Down:
                targetPosition += Vector2.down * moveSpeed * Time.fixedDeltaTime;
                break;
        }

        enemyRigidbody.MovePosition(targetPosition);
    }
    private void FlipMoveDirection()
    {
        if (moveDirection == Direction.Up)
        {
            
            moveDirection = Direction.Down;
        }
        else
        {
            
            moveDirection = Direction.Up;
        }
    }

    private IEnumerator EnemyPatrol()
    {
        while (enemyPatrolling)
        {
            float counter = 0f;
            while (counter < patrolTime)
            {
                counter += Time.fixedDeltaTime;
                Move();
                yield return new WaitForFixedUpdate();
            }
            yield return new WaitForSeconds(5);
            FlipMoveDirection();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // Start the Coroutine for the enemy 
        StartCoroutine(EnemyPatrol());
    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }
}
