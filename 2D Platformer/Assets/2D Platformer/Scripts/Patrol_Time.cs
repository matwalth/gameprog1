using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol_Time : MonoBehaviour
{

    [Header("Enemy Movement")]
    [SerializeField] private float moveSpeed = 0.8f;
    [SerializeField] private float patrolTime = 2;


    private enum Direction
    {
        Right, Left
    }
    private Direction moveDirection = Direction.Right;
    [SerializeField] private bool enemyPatrolling = true;

    [Header("Enemy Animation")]
    [SerializeField] private Rigidbody2D enemyRigidbody;
    [SerializeField] private BoxCollider2D boxCollider2D;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private LayerMask groundLayer;
    private bool obstacle = false;
    private void Move()
    {
        Vector2 targetPosition = transform.position;

        switch (moveDirection)
        {
            default:
            //targetPosition += (points[index] - targetPosition) * moveSpeed * Time.fixedDeltaTime;
            //break;
            case Direction.Right:
                
                //if (!checkObstacleRight())
                //{
                    targetPosition += Vector2.right * moveSpeed * Time.fixedDeltaTime;
                //}
                break;
            case Direction.Left:
                //if (!checkObstacleLeft())
                //{
                    targetPosition -= Vector2.right * moveSpeed * Time.fixedDeltaTime;

                //}
                break;
        }

        enemyRigidbody.MovePosition(targetPosition);
    }
    private bool checkObstacleRight()
    {
        RaycastHit2D boxCastHitRight = Physics2D.BoxCast(
                boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f,
                Vector2.right, .1f, groundLayer);
        //This returns null if the box cast failed
        return (boxCastHitRight.collider != null);
    }
    private bool checkObstacleLeft()
    {
        RaycastHit2D boxCastHitLeft = Physics2D.BoxCast(
                boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f,
                Vector2.left, .1f, groundLayer);
        //This returns null if the box cast failed
        return (boxCastHitLeft.collider != null);
    }
    private void FlipMoveDirection()
    {
        //ebug.Log("Flipping");
        if (moveDirection == Direction.Right)
        {
            moveDirection = Direction.Left;
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }
        else
        {
            moveDirection = Direction.Right;
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }
    }

    private IEnumerator EnemyPatrol()
    {
        while (enemyPatrolling)
        {
            float counter = 0f;
            obstacle = false;
            while (counter < patrolTime && !obstacle)
            {
                counter += Time.fixedDeltaTime;
                //Debug.Log(checkObstacleRight());
                if ((moveDirection == Direction.Right && checkObstacleRight()) || obstacle)
                {
                    yield return new WaitForFixedUpdate();
                    obstacle = true;
                    //Debug.Log("Right Obstacle");
                }
                else if ((moveDirection == Direction.Left && checkObstacleLeft()) || obstacle)
                {
                    yield return new WaitForFixedUpdate();
                    obstacle = true;
                    //Debug.Log("Left Obstacle");
                }
                else
                {
                    Move();
                    obstacle = false;
                    yield return new WaitForFixedUpdate();
                    //Debug.Log("No Obstacle");
                }
            }
            yield return new WaitForSeconds(5);
            FlipMoveDirection();
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Obstacle"))
        {
            obstacle = true;
            //Debug.Log("Hit Obstacle");
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        // Start the Coroutine for the enemy 
        StartCoroutine(EnemyPatrol());
    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }
}
