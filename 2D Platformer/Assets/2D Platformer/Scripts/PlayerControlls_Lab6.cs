using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using UnityEngine.Events;

public class PlayerControlls_Lab6 : MonoBehaviour
{
    [SerializeField] private PlayerInput playerInput;
    // Start is called before the first frame update
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private Rigidbody2D body;
    [SerializeField]
    private SpriteRenderer sprite;
    [SerializeField]
    private BoxCollider2D boxCollider2D;
    [SerializeField]
    private LayerMask environmentLayerMask;
    [SerializeField]
    private LayerMask wallLayer;
    [SerializeField]
    private float jumpForce;

    private bool isGrounded;
    private bool isWallGrounded;
    private bool wallCollision;
    private Vector2 moveInput;

    [Tooltip("Maximum force that can be applied to an object")]
    [SerializeField]
    private float moveSpeed;
    [SerializeField]
    private float frictionAmount;
    private Keyboard key;
    //Create a public property for MaxForce
    public float MoveSpeed { get { return moveSpeed; } set { moveSpeed = value; } }

    [Tooltip("Amount of acceleration to apply to get to moveSpeed")]
    [SerializeField]
    private float moveAcceleration;
    private float moveForce;

    private bool pauseState = false;
    [SerializeField]
    private GameObject pauseUI;
    [SerializeField] private GameObject hud;
    // Start is called before the first frame update
    void Awake()
    {
        if (animator == null || body == null)
        {
            Debug.LogError("You forgot to set the Animator or RigidBody2D");
        }
        moveForce = body.mass * moveAcceleration;
    }
    private void Start()
    {
        key = Keyboard.current;
    }
    // Update is called once per frame
    void Update()
    {
        CheckGrounded();
        CheckWallGrounded();
        CheckWall();
    }
    // Set up to change the move force while game is running
    private void OnValidate()
    {
        moveForce = body.mass * moveAcceleration;
    }
    public void MoveActionPerformed(InputAction.CallbackContext context)
    {
        //Extract the x value
        moveInput = new Vector2(context.ReadValue<Vector2>().x, 0);

    }
    private void FixedUpdate()
    {
        //
        Move(moveInput);

        CheckRunning();
    }
    private void Move(Vector2 direction)
    {
        //Only move if we have a value not 0 for direction
        if (!Mathf.Approximately(direction.x, 0))
        {
            //Calculate the maximum speed difference
            float speedDiff = moveSpeed - Mathf.Abs(body.velocity.x);
            //We can still apply force if we haven't reached max velocity
            if (!Mathf.Approximately(speedDiff, 0))
            {
                if (speedDiff > 0)
                {
                    float accelCap = Mathf.Min(speedDiff / Time.fixedDeltaTime * body.mass,
                                                moveForce);
                    body.AddForce(direction * accelCap, ForceMode2D.Force);
                }
                // In this case we are moving too fast
                else if (speedDiff < 0)
                {
                    body.AddForce(new Vector2(speedDiff * Mathf.Sign(body.velocity.x), 0),
                                    ForceMode2D.Impulse);
                }
            }
        }
        else if (isGrounded)
        {
            float amount = Mathf.Min(Mathf.Abs(body.velocity.x), Mathf.Abs(frictionAmount));

            amount *= Mathf.Sign(body.velocity.x);

            //Apply a breaking impulse to the player's velocity
            body.AddForce(Vector2.right * -amount * body.mass, ForceMode2D.Impulse);
        }

        //Changes sprite render direction
        if (key.aKey.isPressed)
        {
            sprite.flipX = true;
        }
        else if (key.dKey.isPressed)
        {
            sprite.flipX = false;
        }
    }
    private void CheckWall()
    {
        RaycastHit2D boxCastHitRight = Physics2D.BoxCast(
                boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f,
                Vector2.right, .1f, wallLayer);
        RaycastHit2D boxCastHitLeft = Physics2D.BoxCast(
                boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f,
                Vector2.left, .1f, wallLayer);
        //This returns null if the box cast failed
        wallCollision = (boxCastHitRight.collider != null) || (boxCastHitLeft.collider != null);

        //Set the IsGrounded Parameter in the animator
        animator.SetBool("WallCollision", wallCollision);
    }
    private void CheckRunning()
    {
        animator.SetFloat("MovingSpeed",
                            Mathf.Abs(body.velocity.x));
    }

    private void CheckWallGrounded()
    {
        RaycastHit2D boxCastHitRight = Physics2D.BoxCast(
                boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f,
                Vector2.right, .1f, wallLayer);
        RaycastHit2D boxCastHitLeft = Physics2D.BoxCast(
                boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f,
                Vector2.left, .1f, wallLayer);
        //This returns null if the box cast failed
        isWallGrounded = (boxCastHitRight.collider != null) || (boxCastHitLeft.collider != null);

        //Set the IsGrounded Parameter in the animator
        animator.SetBool("IsWallGrounded", isWallGrounded);
    }
    private void CheckGrounded()
    {
        RaycastHit2D boxCastHit = Physics2D.BoxCast(
                boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f,
                Vector2.down, .1f, environmentLayerMask);
        //This returns null if the box cast failed
        isGrounded = (boxCastHit.collider != null);

        //Set the IsGrounded Parameter in the animator
        animator.SetBool("IsGrounded", isGrounded);
    }
    public void Jump(InputAction.CallbackContext context)
    {
        // Jump was pressed
        if (context.performed)
        {
            if (isGrounded || isWallGrounded)
            {
                //Adding force to jump
                body.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);

                //Sets the trigger for jumping
                animator.SetTrigger("Jump");
            }
        }
        else if (context.canceled)
        {
            //Cancel the jump if she is jumping
            if (body.velocity.y > 0)
            {
                body.AddForce(Vector2.down * body.velocity.y * .5f * body.mass, ForceMode2D.Impulse);

                //Sets trigger for falling
                animator.SetTrigger("Fall");
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            Kill();
        }
    }
    public void Kill()
    {
        Debug.Log("You were killed!");
    }

    public void OnPlayerPause()
    {
        //if (context.performed)
        //{
            if (!pauseState)
            {
                SwitchCurrentActionMap("UI");
                //GetComponent<PauseMenuController>().OnPlayerPause();
                pauseUI.GetComponent<PauseMenuController>().OnPlayerPause();
                pauseState = true;
                hud.GetComponent<HUDController>().OnPlayerPause();
            }
            /*else
            {
                SwitchCurrentActionMap("Player");
                pauseUI.GetComponent<PauseMenuController>().OnPlayerPause();
                pauseState = false;
            }*/
        //}
    }
    public void EscapePause()
    {
        if (pauseState)
        {
            SwitchCurrentActionMap("Player");
            //GetComponent<PauseMenuController>().OnPlayerPause();
            pauseUI.GetComponent<PauseMenuController>().EscapePause();
            pauseState = false;
            hud.GetComponent<HUDController>().EscapePause();
        }
    }

    private void SwitchCurrentActionMap(string mapName)
    {
        //Disable the current action map
        playerInput.currentActionMap.Disable();
        //Switch to new action map
        playerInput.SwitchCurrentActionMap(mapName);

        switch (mapName)
        {
            case "UI":
                UnityEngine.Cursor.visible = true;
                UnityEngine.Cursor.lockState = CursorLockMode.None;
                break;
            default:
                UnityEngine.Cursor.visible = false;
                UnityEngine.Cursor.lockState = CursorLockMode.Locked;
                break;
        }
    }
}
