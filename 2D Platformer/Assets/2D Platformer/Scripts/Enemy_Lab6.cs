using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Lab6 : MonoBehaviour
{
    [Header("Enemy Animation")]
    [SerializeField] private Rigidbody2D enemyRigidbody;
    [SerializeField] private BoxCollider2D boxCollider2D;



    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerControlls_Lab6>().Kill();
        }
    }
}
